import copy
import datetime
import json
import argparse
import tqdm
import cube_comm

STATUS_BAR_FORMAT = "Position: {postfix[1][0]:3.1f} {postfix[1][1]:3.1f} {postfix[1][2]:3.1f} | \
Measured: {postfix[2][0]:4.1f} {postfix[2][1]:4.1f} {postfix[2][2]:4.1f} | Status: {postfix[0]}"

PROGRESS_BAR_FORMAT = "{percentage:3.1f}%|{bar}| {n_fmt}/{total_fmt} [{elapsed}, rem:{remaining}, {rate_fmt}]"

# Y/n verify from user, defaults to Yes
def verify(prompt):
    valid = {"yes":True, "y":True, "no":False, "n":False}
    prompt += " [Y/n]:"
    while True:
        choice = input(prompt).lower()
        if choice == '': 
            return True
        elif choice in valid: 
            return valid[choice]
        else: 
            print("Please respond with yes/no or y/n")


# check if config has all the required fields
def check_config(config):
    field = ["name", "output", "calibration", "start", "steps", "step_size"]
    for item in field:
        if item not in config:
            print("Config file missing field:", item)
            return False
    return True


def single_measurement(verbose, status, bars, cube, pos):
    # move the cube, handle errors
    hasError, error, data = cube.move(pos[0], pos[1], pos[2])
    if hasError:
        status[0] = error
        if verbose: print(error)
        else:
            for bar in bars: bar.update(1)
        return []
    
    # single measure, handle errors
    hasError, error, data = cube.measure()
    if hasError:
        status[0] = error
        if verbose: print(error)
        else:
            for bar in bars: bar.update(1)
        return []
    
    # update status bar
    status[0] = "OK"
    status[1] = data[0]
    status[2] = data[1]
    if verbose: print(data)
    else:
        for bar in bars: bar.update(1)
    return data


def calibrate(config, cube, verbose):
    calibration = []
    current = copy.copy(config['start'])
    status = {0: "", 1: [0, 0, 0], 2:[0, 0, 0]}

    # setup status and progress bars
    if not verbose:
        sbar = tqdm.tqdm(bar_format=STATUS_BAR_FORMAT,
                         postfix=status, position=0)
        pbar = tqdm.tqdm(bar_format=PROGRESS_BAR_FORMAT,
                         total=config['steps'][0] * config['steps'][1], position=1)
    else: 
        sbar = None
        pbar = None
    
    # go through the first XY plane, saving the values
    for y in range(config['steps'][1]):
        line = []
        for x in range(config['steps'][0]):
            data = single_measurement(verbose, status, (pbar, sbar), cube, current)
            # if measurement fails, try until it succeeds
            while  len(data) == 0:
                if not verbose: pbar.total += 1
                data = single_measurement(verbose, status, (pbar, sbar), cube, current)
            line.append(data[1])
            current[0] += config['step_size'][0]
        calibration.append(line)
        current[0] = config['start'][0]
        current[1] += config['step_size'][1]
    
    if not verbose:
        sbar.close()
        pbar.close()
    return calibration


def measure(config, cube, verbose, calibration):
    # setup the file header
    config['datetime'] = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")
    current = copy.copy(config['start'])
    output_file = open(config['output'], "w")
    output_file.write("METADATA:\n")
    output_file.write(str(config) + '\n')
    output_file.write("---------\n")

    # setup the status and progress bars
    status = {0: "", 1: [0, 0, 0], 2: [0, 0, 0]}
    if not verbose:
        sbar = tqdm.tqdm(bar_format=STATUS_BAR_FORMAT,
                         postfix=status, position=0)
        pbar = tqdm.tqdm(bar_format=PROGRESS_BAR_FORMAT,
                         total=config['steps'][0] * config['steps'][1] * config['steps'][2], position=1)
    else: 
        sbar = None
        pbar = None
    
    # go through all points
    for z in range(config['steps'][2]):
        for y in range(config['steps'][1]):
            for x in range(config['steps'][0]):
                # if measurement fails, try until it succeeds
                data = single_measurement(verbose, status, (pbar, sbar), cube, current)
                while  len(data) == 0:
                    if not verbose: pbar.total += 1
                    data = single_measurement(verbose, status, (pbar, sbar), cube, current)
                # write position
                output_file.write(str(data[0][0]) + ', ')
                output_file.write(str(data[0][1]) + ', ')
                output_file.write(str(data[0][2]) + ', ')
                # write data, accounting for possible calibration data
                if config['calibration']:
                    output_file.write(str(data[1][0] - calibration[y][x][0]) + ', ')
                    output_file.write(str(data[1][1] - calibration[y][x][1]) + ', ')
                    output_file.write(str(data[1][2] - calibration[y][x][2]) + '\n')
                else:
                    output_file.write(str(data[1][0]) + ', ')
                    output_file.write(str(data[1][1]) + ', ')
                    output_file.write(str(data[1][2]) + '\n')

                current[0] += config['step_size'][0]
            current[0] = config['start'][0]
            current[1] += config['step_size'][1]
        current[0] = config['start'][0]
        current[1] = config['start'][1]
        current[2] += config['step_size'][2]
    
    # clean up at the end
    output_file.close()

    if not verbose:
        sbar.close()
        pbar.close()
    return

def main(arguments):
    verbose = arguments['verbose']
    if verbose: print("Loading config:", arguments['CONFIG'])
    with open(arguments['CONFIG']) as f:
        config = json.load(f)
        if verbose: print("Config loaded")
    
    if verbose: print("Checking config...", end="")
    if not check_config(config):
        print("Invalid config file, exiting.")
        exit()
    if verbose: print("OK")

    print("Loaded config data:")
    for key, value in config.items():
        print(key,":", value)
    if not verify("Continue?"):
        print("Exiting.")
        exit()
    
    if verbose: print("Opening cube connection:", arguments['PORT'])
    cube = cube_comm.CubeComm(arguments['PORT'])
    if verbose: print("Connection established.")
    
    calibration = None
    if config['calibration']:
        print("Starting calibration, please remove/turn off all magnetic sources.")
        while not verify("Continue?"):
            continue
        calibration = calibrate(config, cube, verbose)
    
    print("Calibration done, please add/turn on all magnetic sources.")
    while not verify("Continue?"):
        continue

    measure(config, cube, verbose, calibration)
    print("Measuring done. Exiting.")
    exit()
    

if __name__ == "__main__":
    # cmd arg parsing
    parser = argparse.ArgumentParser(
            description = "Tool for executing a measurement with Cube and storing the measured data.",
            usage = "measure_tool.py CONFIG PORT [-v] [-h] [-n]"
    )
    parser.add_argument('CONFIG',
            help = "Config file that includes the measurement parameters."
    )
    parser.add_argument('PORT',
            help = "Serial port on which Cube is present."
    )
    parser.add_argument('-v', '--verbose',
            action = 'store_true',
            help = "Toggle more verbose command line output."
    )
    parser.add_argument('--version',
            action = 'version',
            version = '%(prog)s 0.1'
    )
    arguments = vars(parser.parse_args())
    main(arguments)