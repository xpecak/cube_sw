import sys
import cmd
import cube_comm



def decorate(input_string, color, bold):
    if (bold):
        input_string = "\u001b[1m" + input_string
    if (color == "red"):
        return "\u001b[31m" + input_string + "\u001b[0m"
    if (color == "blue"):
        return "\u001b[34m" + input_string + "\u001b[0m"
    if (color == "green"):
        return "\u001b[32m" + input_string + "\u001b[0m"
    if (color == "magenta"):
        return "\u001b[35m" + input_string + "\u001b[0m"
    return input_string + "\u001b[0m"


class CubeConsole(cmd.Cmd):
    prompt = ">> "
    ruler = "-"
    intro = decorate("Cube Control Console\n", "blue", True) +\
            decorate("type 'help' or '?' for help.\n", "blue", False)
    doc_header = "Commands (type help <command>):"
    cube = ""

    def __init__(self, cube):
        super().__init__()
        self.cube = cube

    def default(self, line):
        print(decorate("unknown command:", "red", True), line)

    def emptyline(self):
        return

    def do_exit(self, args):
        """exit the console"""
        return True

    def do_EOF(self, args):
        """exit the console"""
        print("EOF")
        return True

    def do_move(self, args):
        """move to coord\n    syntax: move X Y Z"""
        split_args = args.split()
        if (len(split_args) != 3):
            print(decorate("Wrong arguments!", "red", True))
            return
        coord = list(map(float, split_args))
        has_error, error, data = self.cube.move(coord[0], coord[1], coord[2])
        if (has_error):
            print(decorate(error, "red", False))
            return
        print("moved successfully")
    
    def do_measure(self, args):
        """take measurement"""
        has_error, error, data = self.cube.measure()
        if (has_error):
            print(decorate(error, "red", False))
            return
        print("Relative XYZ:" + str(data[0]))
        print("Measured XYZ:" + str(data[1]))

    
    def do_status(self, args):
        """get cube status"""
        has_error, error, data = self.cube.status()
        if (has_error):
            print(decorate(error, "red", False))
            return
        print("status OK")
    
    def do_home(self, args):
        """Home cube"""
        has_error, error, data = self.cube.home()
        if (has_error):
            print(decorate(error, "red", False))
            return
        print("Homed successfully")

    def do_zero(self, args):
        """set current position as zero"""
        has_error, error, data = self.cube.zero()
        if (has_error):
            print(decorate(error, "red", False))
            return
        print("current position set to zero")

    def do_relative(self, args):
        """get current relative position"""
        has_error, error, data = self.cube.relative()
        if (has_error):
            print(decorate(error, "red", False))
            return
        print("Relative XYZ: " + str(data))

    def do_absolute(self, args):
        """get current absolute position"""
        has_error, error, data = self.cube.absolute()
        if (has_error):
            print(decorate(error, "red", False))
            return
        print("Absolute XYZ: " + str(data))

if __name__ == "__main__":
    if (len(sys.argv) < 2):
        print("Missing COM port path")
        exit(1)
    cube = cube_comm.CubeComm(sys.argv[1])
    console = CubeConsole(cube)
    console.cmdloop()