import serial
import protocols_pb2

class CubeComm():
    ser = None
    def __init__(self, ser_path):
        self.ser = serial.Serial(ser_path, 115200, timeout = 0.1)
        self.ser.flush()

    def __sendData(self, data):
        msg_out = [0x55, 0x55, 0x55, len(data)]
        msg_out.extend(data)
        self.ser.write(msg_out)

    def __receiveData(self):
        receive = []
        while (len(receive) == 0):
            receive = self.ser.read(200)
        return receive

    def __readPacket(self, data):
        found = []
        header_found = 0
        data_length = 0
        reading_data = False
        for i in range(len(data)):
            if not reading_data:
                if header_found == 3:
                    data_length = data[i]
                    reading_data = True
                    continue
                if data[i] != 0x55:
                    header_found = 0
                    continue
                if data[i] == 0x55:
                    header_found += 1
                    continue
            if reading_data:
                if data_length == 0:
                    return (found, True)
                found.append(data[i])
                data_length -= 1
        if (data_length == 0):
            return (found, True)
        return (found, False)

    def __decodeReceive(self, received, command):
        data, found_end = self.__readPacket(received)
        if not found_end:
            return (True, "Data read error", [])
        msg = protocols_pb2.Return_msg().FromString(bytearray(data))
        has_error, error = self.__decodeStatus(msg, command)
        data_out = [None, None]
        if msg.HasField('position'):
            data_out[0] = (msg.position.x, msg.position.y, msg.position.z)
        if msg.HasField('measurement'):
            data_out[1] = (msg.measurement.x, msg.measurement.y, msg.measurement.z)
        return (has_error, error, tuple(data_out))

    def __decodeStatus(self, msg, command):
        if msg.error == protocols_pb2.PLANNER_ERROR:
            return (True, "Planner Error #" + str(msg.errorCode))
        if msg.error == protocols_pb2.STEPPER_ERROR:
            return (True, "Stepper Error #" + str(msg.errorCode))
        if msg.error == protocols_pb2.SENSOR_ERROR:
            return (True, "Sensor Error #" + str(msg.errorCode))
        if msg.error == protocols_pb2.MISC_ERROR:
            return (True, "Misc Error #" + str(msg.errorCode))
        if msg.cmdAck != command:
            return (True, "Unrecognized command by cube")
        return (False, None)

    
    def __simpleCommand(self, command):
        msg = protocols_pb2.Command_msg()
        msg.command = command
        self.__sendData(msg.SerializeToString())
        data_in = self.__receiveData()
        return self.__decodeReceive(data_in, command)

    def move(self, x, y, z):
        msg = protocols_pb2.Command_msg()
        msg.command = protocols_pb2.MOVETO
        msg.position.x = x
        msg.position.y = y
        msg.position.z = z
        self.__sendData(msg.SerializeToString())
        data_in = self.__receiveData()
        return self.__decodeReceive(data_in, protocols_pb2.MOVETO)

    def zero(self):
        return self.__simpleCommand(protocols_pb2.SET_ZERO_POS)

    def status(self):
        return self.__simpleCommand(protocols_pb2.STATUS)

    def absolute(self):
        return self.__simpleCommand(protocols_pb2.GET_ABS_POS)

    def relative(self):
        return self.__simpleCommand(protocols_pb2.GET_REL_POS)

    def measure(self):
        return self.__simpleCommand(protocols_pb2.MEASURE)
    
    def home(self):
        return self.__simpleCommand(protocols_pb2.HOME)