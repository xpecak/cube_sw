# Cube's software

This repo contains the software to control the Cube via virtual com-port over USB and a simple tool to launch Mayavi visualizations.

- `cube_comm.py` - library for communication with Cube
- `cube_console.py` - console tool for manual control of Cube
- `measure_tool.py` - is a command-line tool for automating measurement proces and saving data
- `vis_launch.py` - tool for setting up and launching visualization

Everything is licensed under the MIT license, details in `LICENSE.md`

## Required Python version and packages
- Python 3.5+(tested on Python 3.6.9, 3.7.7)
- [pyserial](https://pypi.org/project/pyserial/)
- [protobuf](https://pypi.org/project/protobuf/)
- [tqdm](https://pypi.org/project/tqdm/)
- [VTK](https://pypi.org/project/vtk/)
- [PyQt5](https://pypi.org/project/PyQt5/)
- [Mayavi](https://pypi.org/project/mayavi/)

## `cube_comm.py` usage
Required packages: `pyserial`, `protobuf`

Provides a `CubeComm` class.
This class provides an interface for communication with the hardware part of Cube, handling all the decoding and encoding of data.
It requires the serial port of the Cube on initialization.
The public methods map 1:1 to commands supported by Cube.
They are blocking, only returning after error happens or on successfuly finished command.
The commands and respective functions are:
- `STATUS` : `CubeComm.status()`
- `MOVETO` : `CubeComm.move(x, y, z)`
- `MEASURE` : `CubeComm.status()`
- `GET_REL_POS` : `CubeComm.relative()`
- `GET_ABS_POS` : `CubeComm.absolute()`
- `SET_ZERO_POS` : `CubeComm.zero()`
- `HOME` : `CubeComm.home()`

The functions all have the same return type, a tuple: `(has_error, error, data)`.
- `has_error` - indicates whether command resulted in error, `True`/`False`.
- `error` - if `has_error` is `True`, contains string with error detail, in format: `{Failed section} #{Error ID}`.
`None` when `has_error` is `False`.
- `data` - A tuple `(position, measured)`. 
    - `position` is a tuple `(x, y, z)` of relative position of Cube if the command returns this data, otherwise it is `None`. Units are millimeters.
    - `measured` is a tuple `(x, y, z)` of vector components of measured data if the command returns this data, otherwise it is `None`. Units are uT (micro tesla).

Commands that return position data are: `MEASURE`, `GET_REL_POS`, `GET_ABS_POS`.

Command that returns measured data is `MEASURE`.

Example usage can be best seen in `cube_console.py`.

## `cube_console.py` usage
Launch: `$  python cube_console.py PORT`

`PORT` is the path to serial port where cube is connected. Usually `\dev\tty*` on Linux and `COM*` on windows.

The packages `pyserial` and `protobuf` are required to launch this app.

This app provides a simple tool for manual cube control. It acts as a console, which accepts following commands:
- `help CMD` - Without arguments, prints all commands. With argument that is a valid command, prints short help for the command.
- `exit` - Exits the application. Sending EOF also works.
- `home` - Homes the cube via the homing switches, reseting both absolute and relative positions to zero.
- `status` - Prints the status of the cube, indicating any error
- `absolute` - Prints the current absolute position of the head
- `relative` - Prints the current relative position of the head
- `zero` - Sets the current absolute position as the relative zero position
- `move X Y Z` - Moves the cube to relative position X Y Z, requires all three arguments. Unit is milimeters.
- `measure` - Does a single measurement with sensor and prints the result. The result is printed in following format:
```
Relative XYZ: position, in mm
Measured XYZ: measured vector components, in uT
```

If the command ends successfuly, it prints its expected output message or success message if it does not have output.

Failed command prints error message, usually in format of: `{Failed section} #{Error ID}`.

## `measure_tool.py` usage
Launch: `$  python measure_tool.py CONFIG PORT [FLAGS]`

`CONFIG` is path to a JSON file with measurement parameters.

`PORT` is the path to serial port where cube is connected. Usually `\dev\tty*` on Linux and `COM*` on windows.

The only possible flag is `-v` or `--verbose`. It toggles a more verbose console output during setup and measurement.

The packages `pyserial`, `protobuf` and `tqdm` are required.

The app loads the measurement parameters and guides the user through the optional calibration and measurement process. It saves the measurement result and metadata to a .csv file.
The calibration process measures a reference zero data and then saves the output data as a difference to the reference data. Without, the data is saved directly as measured.

The measurement pattern is a regular grid with, origin, size and spacing defined in the config file.

The JSON config file has following format:
```
{
    "name": "measurement name",    //name of the measurement
    "output": "output_file.csv",   //output file of the data
    "calibration": true,           //whether the calibration process should be done before measurement
    "start": [107.5, 130, 7],      //relative X Y Z start point of the measurement
    "steps": [21, 11, 11],         //number of steps per X Y Z
    "step_size": [1, 1, 1]         //size of step in X Y Z, unit is millimeters
}
```

The output .csv file has following format:
```
METADATA:
{json from config file, with added datetime field}
---------
x_pos, y_pos, z_pos, x_measured, y_measured, z_measured
x_pos, y_pos, z_pos, x_measured, y_measured, z_measured
x_pos, y_pos, z_pos, x_measured, y_measured, z_measured
x_pos, y_pos, z_pos, x_measured, y_measured, z_measured
...
...
...
```
Units are millimeters for position and uT for measured data.
Note that the position is relative.

## `vis_launch.py` usage
Launch: `$  python vis_launch.py DATA [FLAGS]`

Requires `VTK`, `PyQt5` and `Mayavi` packages.

This app loads the data from file with same format as the output file from `measure_tool.py` and loads them into a Mayavi visualization. It prepares the vector data source, a vector normal data source and the three scalar sources of the vector components.

`DATA` is the path to the datafile to load.

It has few flags to make setting up the visualization faster.

`-v, --vectors` - force the rendering of quiver plot. 
By default, if any flags are specified, the quiver plot is not rendered. 
This forces the rendering.

`-f, --fieldlines` - render the field lines (streamlines). 
It defaults to a x-axis oriented plane source, with grid size of 15. 
The position of the seed still needs to be adjusted manually for best results.

`-i NUM, --isosurf NUM` - renders isosurfaces(equipotentional surfaces), with NUM evenly spaced levels. 
Defaults to 5 levels if not NUM is specified.

`-s SLICE, --slice SLICE` - Add a slice. Multiple slices can be added by repeating the argument. The format is two letters, where the first letter is the axis of slice.
Second letter is the vector component to visualize.
`s` is the scalar size of the vector. 
If the second character is omited, the slice is a vector field slice. 
EXAMPLE: `-s xy -s x -s zx`
